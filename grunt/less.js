module.exports = function (grunt) {

	var targets  = ["app/less/**/*.less"];

	return {
		dev : {
			options: {
				compress: true
			},
			files: {
				"app/main.css": targets
			}
		}
	};
};
