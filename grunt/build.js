module.exports = function(grunt){

	grunt.task.registerTask('build', [
			'less:' + global.buildType,
			'connect:' + global.buildType
		]);

	grunt.task.registerTask('default', ['watch:dev']);
};
