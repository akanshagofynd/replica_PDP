module.exports = function (grunt) {
	return {
		dev: {
			options: {
		        port: 8787,
		        base: 'app',
				keepalive: true
		    }
		}
	};
};
