module.exports = function(grunt) {
    var buildType = grunt.option('env');
    global.buildType = buildType || 'dev';
    require('load-grunt-config')(grunt, {
        init: true,
        loadGruntTasks: {
            pattern: 'grunt-*',
        },
    });
};
